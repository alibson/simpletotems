if select(2, UnitClass('player')) ~= "SHAMAN" then
	DisableAddOn("SimpleTotems")
	return
end

SimpleTotems = LibStub("AceAddon-3.0"):NewAddon("SimpleTotems", "AceConsole-3.0", "AceEvent-3.0", "AceTimer-3.0", "LibSink-2.0")
local L = LibStub("AceLocale-3.0"):GetLocale("SimpleTotems")
local LBF = LibStub('LibButtonFacade', true)

local UnitBuff = _G.UnitBuff
local GetSpellInfo = _G.GetSpellInfo
local GetTotemInfo = _G.GetTotemInfo
local GetTime = _G.GetTime

local totemTable = { -- [texture] = {name, pulse, hasAura}
	-- Earth totems
	[ [[Interface\Icons\Spell_Nature_StrengthOfEarthTotem02]] ]	= {	GetSpellInfo(2484),		3,		false	}, -- Earthbind Totem
	[ [[Interface\Icons\Spell_Nature_StoneSkinTotem]] ]			= {	GetSpellInfo(8071),		false,	true	}, -- Stoneskin Totem
	[ [[Interface\Icons\Spell_Nature_StoneClawTotem]] ]			= {	GetSpellInfo(5730),		2,		false	}, -- Stoneclaw Totem
	[ [[Interface\Icons\Spell_Nature_EarthBindTotem]] ]			= {	GetSpellInfo(8075),		false,	true	}, -- Strength of Earth Totem
	[ [[Interface\Icons\Spell_Nature_TremorTotem]] ]			= {	GetSpellInfo(8143),		3,		false	}, -- Tremor Totem
	[ [[Interface\Icons\Spell_Nature_EarthElemental_Totem]] ]	= {	GetSpellInfo(2062),		false,	false	}, -- Earth Elemental Totem
	-- Fire totems
	[ [[Interface\Icons\Spell_Fire_SearingTotem]] ]				= {	GetSpellInfo(3599),		false,	false	}, -- Searing Totem
	[ [[Interface\Icons\Spell_Fire_SealOfFire]] ]				= {	GetSpellInfo(1535),		false,	false	}, -- Fire Nova Totem
	[ [[Interface\Icons\Spell_Fire_SelfDestruct]] ]				= {	GetSpellInfo(8187),		2,		false	}, -- Magma Totem
	[ [[Interface\Icons\Spell_FrostResistanceTotem_01]] ]		= {	GetSpellInfo(8181),		false,	true	}, -- Frost Resistance Totem
	[ [[Interface\Icons\Spell_Nature_GuardianWard]] ]			= {	GetSpellInfo(8227),		5,		false	}, -- Flametongue Totem
	[ [[Interface\Icons\Spell_Fire_TotemOfWrath]] ]				= {	GetSpellInfo(30706),	false,	true	}, -- Totem of Wrath
	[ [[Interface\Icons\Spell_Fire_Elemental_Totem]] ]			= {	GetSpellInfo(2894),		false,	false	}, -- Fire Elemental Totem
	-- Water totems
	[ [[Interface\Icons\INV_Spear_04]] ]						= {	GetSpellInfo(5394),		false,	true	}, -- Healing Stream Totem
	[ [[Interface\Icons\Spell_Nature_ManaRegenTotem]] ]			= {	GetSpellInfo(5675),		false,	true	}, -- Mana Spring Totem
	[ [[Interface\Icons\Spell_Nature_PoisonCleansingTotem]] ]	= {	GetSpellInfo(8166),		5,		false	}, -- Poison Cleansing Totem
	[ [[Interface\Icons\Spell_Nature_DiseaseCleansingTotem]] ]	= {	GetSpellInfo(8170),		5,		false	}, -- Disease Cleansing Totem
	[ [[Interface\Icons\Spell_Frost_SummonWaterElemental]] ]	= {	GetSpellInfo(16190),	3,		true	}, -- Mana Tide Totem
	[ [[Interface\Icons\Spell_FireResistanceTotem_01]] ]		= {	GetSpellInfo(8184),		false,	true	}, -- Fire Resistance Totem
	-- Air totems
	[ [[Interface\Icons\Spell_Nature_InvisibilityTotem]] ]		= {	GetSpellInfo(8835),		false,	true	}, -- Grace of Air Totem
	[ [[Interface\Icons\Spell_Nature_Windfury]] ]				= {	GetSpellInfo(8512),		5,		false	}, -- Windfury Totem
	[ [[Interface\Icons\Spell_Nature_GroundingTotem]] ]			= {	GetSpellInfo(8177),		false,	true	}, -- Grounding Totem
	[ [[Interface\Icons\Spell_Nature_EarthBind]] ]				= {	GetSpellInfo(15107),	false,	true	}, -- Windwall Totem
	[ [[Interface\Icons\Spell_Nature_RemoveCurse]] ]			= {	GetSpellInfo(6495),		false,	true	}, -- Sentry Totem
	[ [[Interface\Icons\Spell_Nature_NatureResistanceTotem]] ]	= {	GetSpellInfo(10595),	false,	true	}, -- Nature Resistance Totem
	[ [[Interface\Icons\Spell_Nature_Brilliance]] ]				= {	GetSpellInfo(25908),	false,	true	}, -- Tranquil Air Totem
	[ [[Interface\Icons\Spell_Nature_SlowingTotem]] ]			= {	GetSpellInfo(3738),		false,	true	}, -- Wrath of Air Totem
}

local totemOrder	= {2, 1, 3, 4} -- order of totem display, 1-Fire, 2-Earth, 3-Water, 4-Air
local expiryTime	= {false, false, false, false} -- expiry time of totems
local pulseTimer	= {false, false, false, false} -- IDs of scheduled timers
local pulseFreq		= {false, false, false, false} -- pulse frequency
local lastTotem		= {false, false, false, false} -- used for warning
local lastTotemIcon	= {false, false, false, false} -- used for warning and scanning buffs
local scanTable		= {false, false, false, false} -- used for scanning buffs
local durationTimer -- ID of scheduled timer

local totemicCallName = GetSpellInfo(36936)
local warnThrottle = 0
local warningCache = {}

local function FormatTime(sec)
	if sec < 60 then
		return "%d", sec
	else
		return "%d:%02d", sec / 60, sec % 60
	end
end

-------------------------------------------------------------------------------
-- Initialization
-------------------------------------------------------------------------------

function SimpleTotems:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New("SimpleTotemsDB", self.defaults, "Default")
	LibStub("AceConfig-3.0"):RegisterOptionsTable("SimpleTotems", self:GetOptions()); self.GetOptions = nil
	self:RegisterChatCommand("st", function() LibStub("AceConfigDialog-3.0"):Open("SimpleTotems") end )
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("SimpleTotems", "SimpleTotems")
	
	self:SetSinkStorage(self.db.profile.sinkOptions)

	self.db.RegisterCallback(self, "OnProfileChanged", "SetupFrames")
	self.db.RegisterCallback(self, "OnProfileCopied", "SetupFrames")
	self.db.RegisterCallback(self, "OnProfileReset", "SetupFrames")

	-- ButtonFacade
	if LBF then
		LBF:RegisterSkinCallback("SimpleTotems", self.ButtonFacadeCallback, self)
	end
end

function SimpleTotems:OnEnable()
	-- hide blizz frames
	_G["TotemFrame"]:UnregisterAllEvents()
	_G["TotemFrame"].Show = function() end
	_G["TotemFrame"]:Hide()

	self:RegisterEvent("PLAYER_ENTERING_WORLD", "SetupFrames")

	self:RegisterEvent("PLAYER_TOTEM_UPDATE")
	self:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
	self:RegisterEvent("PLAYER_DEAD")
	self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	if self.db.profile.rangeCheck then
		self:RegisterEvent("PLAYER_AURAS_CHANGED", "ScanBuffs")
	end
end

-------------------------------------------------------------------------------
-- Event handlers
-------------------------------------------------------------------------------

function SimpleTotems:PLAYER_TOTEM_UPDATE(_, ID)
	self:SetTotem(ID)
end

function SimpleTotems:UNIT_SPELLCAST_SUCCEEDED(_, unit, spell)
	if spell == totemicCallName and unit == "player" then
		warnThrottle = GetTime() + 1.5 -- don't show warnings about expiring totems after using Totemic Call
	end
end

function SimpleTotems:PLAYER_DEAD()
	warnThrottle = GetTime() + 1.5 -- don't show warnings when player dies
end

function SimpleTotems:COMBAT_LOG_EVENT_UNFILTERED(_, _, event, _, _, sourceFlags)
	if sourceFlags == 0x1111 and event == "SPELL_CAST_START" then -- show Searing Totem casts as pulses
		pulseFreq[1] = 2.2
		self:Pulse(1)
	end
end

-------------------------------------------------------------------------------
-- General functions
-------------------------------------------------------------------------------

function SimpleTotems:ColorWarningString(ID)
	if not warningCache[lastTotem[ID]] then -- memoize expensive string operations
		local db = self.db.profile.totemColors[ID]
		local r, g, b = db[1] * 255, db[2] * 255, db[3] * 255
		warningCache[lastTotem[ID]] = ((L["%s expired"]):gsub("%%s", "|cff%%02x%%02x%%02x%%s|r")):format(r, g, b, lastTotem[ID])
	end
	return warningCache[lastTotem[ID]]
end

function SimpleTotems:SetTotem(ID)
	local _, _, startTime, maxDuration, texture = GetTotemInfo(ID)

	if texture == "" then -- totem slot empty
		expiryTime[ID] = false
		self.frames[ID]:Hide()
		local db = self.db.profile.warning
		if db.show and lastTotem[ID] and warnThrottle < GetTime() then
			self:Pour(
				db.colorName and self:ColorWarningString(ID) or (L["%s expired"]):format(lastTotem[ID]),
				db.color[1],
				db.color[2],
				db.color[3],
				nil, nil, nil, nil, nil,
				db.showIcon and lastTotemIcon[ID] or nil
			)
		end
		lastTotem[ID] = false
		lastTotemIcon[ID] = false
	else
		expiryTime[ID] = startTime + maxDuration
		
		self:CancelTimer(pulseTimer[ID], true) -- cancel previous pulse timer in case of replacing previous totem
		if totemTable[texture][2] then -- if totem pulses
			pulseFreq[ID] = totemTable[texture][2] -- store pulse frequency for Pulse() to lookup, because you can only pass 1 arg to a timer
			self:Pulse(ID) -- start cooldown
			pulseTimer[ID] = self:ScheduleRepeatingTimer("Pulse", pulseFreq[ID], ID) -- schedule timer
		else
			self.frames[ID].cooldown:SetCooldown(0, 0) -- stop previous cooldown
		end
		
		self:UpdateDuration() -- update text when putting down totem instead of waiting for the next timer tick

		self.frames[ID].icon:SetTexture(texture)
		self.frames[ID].icon:SetVertexColor(1, 1, 1)
		self.frames[ID]:Show()

		-- remember last totem, used for warning and scanning buffs
		lastTotem[ID] = totemTable[texture][1]
		lastTotemIcon[ID] = texture
	end
end

function SimpleTotems:UpdateDuration()
	local currentTime = GetTime()

	for i = 1, 4 do
		if expiryTime[i] then
			self.frames[i].duration:SetFormattedText(FormatTime(expiryTime[i] - currentTime))
		end
	end
end

function SimpleTotems:Pulse(ID)
	if expiryTime[ID] then -- if totem still up then start another cooldown
		self.frames[ID].cooldown:SetCooldown(GetTime(), pulseFreq[ID])
	else
		self:CancelTimer(pulseTimer[ID], true)
	end
end

function SimpleTotems:OnClick(ID)
	DestroyTotem(ID)
end

-------------------------------------------------------------------------------
-- Range check
-------------------------------------------------------------------------------

function SimpleTotems:ScanBuffs()
	local totemCount = 0 -- count how many totems to look for
	local icon, _

	-- put buff textures of totems with aura into an array
	for i = 1, 4 do
		icon = lastTotemIcon[i]
		if icon and totemTable[icon][3] then -- if totem up and has aura
			scanTable[i] = icon
			totemCount = totemCount + 1
		else
			scanTable[i] = false
		end
	end

	for i = 1, 40 do
		if totemCount > 0 then -- if totems to find
			_, _, icon = UnitBuff("player", i)
			if icon then
				for i = 1, 4 do
					if icon == scanTable[i] then -- totem buff found
						scanTable[i] = false -- remove from array
						totemCount = totemCount - 1 -- decrement the counter
						self.frames[i].icon:SetVertexColor(1, 1, 1) -- reset color
						break
					end
				end
			else -- no more buffs
				break
			end
		else -- no more totems to find
			break
		end
	end

	if totemCount > 0 then -- if totem auras missing
		for i = 1, 4 do
			if scanTable[i] then
				self.frames[i].icon:SetVertexColor(1, 0, 0)
			end
		end
	end
end

-------------------------------------------------------------------------------
-- ButtonFacade
-------------------------------------------------------------------------------

function SimpleTotems:ButtonFacadeCallback(SkinID, Gloss, Backdrop, Group, Button, Colors)
	local db = self.db.profile

	if not db.ButtonFacade then
		db.ButtonFacade = {SkinID, Gloss, Backdrop, Colors}
	else
		db.ButtonFacade[1] = SkinID
		db.ButtonFacade[2] = Gloss
		db.ButtonFacade[3] = Backdrop
		db.ButtonFacade[4] = Colors
	end

	if db.colorBorders then
		for i = 1, 4 do
			self.frames[i].border:SetVertexColor(unpack(db.totemColors[i]))
		end
	end
end

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------

function SimpleTotems:SetupFrames()
	if not self.frames then
		self.frames = {
			[1] = _G["SimpleTotem1"],
			[2] = _G["SimpleTotem2"],
			[3] = _G["SimpleTotem3"],
			[4] = _G["SimpleTotem4"],
		}
	end
	
	local db = self.db.profile

	self.BaseFrame:ClearAllPoints()
	self.BaseFrame:SetPoint(db.point, "UIParent", db.relativePoint, db.xOffset, db.yOffset)
	self.BaseFrame:SetHeight(db.iconSize + 10)
	self.BaseFrame:SetWidth((db.iconSize * 4) + (db.xSpacing * 3))
	self.BaseFrame:SetScale(db.scale)

	local alignTo = self.BaseFrame
	for i = 1, 4 do
		self:InitTotemIcon(totemOrder[i], alignTo)
		alignTo = self.frames[totemOrder[i]]
	end
	
	-- apply saved ButtonFacade skin
	if LBF and db.ButtonFacade then
		LBF:Group("SimpleTotems"):Skin(unpack(db.ButtonFacade))
	end

	if not durationTimer then
		durationTimer = self:ScheduleRepeatingTimer("UpdateDuration", 1)
	end

	warningCache = {} -- clear old color cache

	self:UnregisterEvent("PLAYER_ENTERING_WORLD")
end

function SimpleTotems:InitTotemIcon(i, alignTo)
	local frame = self.frames[i]
	local db = self.db.profile
	
	local relativePoint = "TOPRIGHT"
	local horizontalOffset = db.xSpacing
	local verticalOffset = db.ySpacing
	if alignTo == self.BaseFrame then
		relativePoint = "TOPLEFT"
		horizontalOffset = 0
		verticalOffset = 0
	end

	frame:SetPoint("TOPLEFT", alignTo, relativePoint, horizontalOffset, verticalOffset)
	frame:SetWidth(db.iconSize)
	frame:SetHeight(db.iconSize)

	--skin buttons
	if LBF then
		LBF:Group("SimpleTotems"):AddButton(frame)
		if db.colorBorders then
			frame.border:SetVertexColor(unpack(db.totemColors[i]))
			frame.border:Show()
		else
			frame.border:Hide()
		end
	else
		if db.borders then
			frame.border:SetTexCoord(0, 1, 0, 1)
			frame.border:SetTexture([[Interface\AddOns\SimpleTotems\border-dark]])
			if db.colorBorders then
				frame.border:SetVertexColor(unpack(db.totemColors[i]))
			else
				frame.border:SetVertexColor(0.5,0.5,0.5,1)
			end
			frame.border:Show()
		else
			frame.border:Hide()
		end
	end

	local fontName = frame.duration:GetFont()
	frame.duration:SetFont(fontName, db.fontSize, db.fontEffect)
	
	if not self.moving then
		self:SetTotem(i)
	end
end

function SimpleTotems:MoveFrame()
	if not self.moving then
		self.BaseFrame:EnableMouse(true)
		for i = 1, 4 do
			self.frames[i]:EnableMouse(false)
			self.frames[i].icon:SetTexture([[Interface\Icons\Spell_Totem_WardOfDraining]])
			self.frames[i].icon:SetVertexColor(1, 1, 1)
			self.frames[i]:Show()
		end
		self:Print(L["Frame unlocked"])
		self.moving = true
	else
		self.BaseFrame:EnableMouse(false)
		for i = 1, 4 do
			self.frames[i]:EnableMouse(true)
			self:SetTotem(i)
		end
		self:Print(L["Frame locked"])
		self.moving = false
	end
end

function SimpleTotems:FinishedMoving(frame)
	frame:StopMovingOrSizing()
	local point, _, relativePoint, xOffset, yOffset = frame:GetPoint()
	self.db.profile.point = point
	self.db.profile.relativePoint = relativePoint
	self.db.profile.xOffset = xOffset
	self.db.profile.yOffset = yOffset
end
