if not SimpleTotems then return end

local L = LibStub("AceLocale-3.0"):GetLocale("SimpleTotems")

SimpleTotems.defaults = {
	profile = {
		borders = true,
		colorBorders = true,
		iconSize = 30,
		fontSize = 10,
		fontEffect = "none",
		scale = 1,
		point = "CENTER",
		relativePoint = "CENTER",
		xOffset = 0,
		yOffset = -300,
		xSpacing = 5,
		ySpacing = 0,
		warning = {
			show = true,
			colorName = true,
			color = { 1, .5, 0 },
			showIcon = true,
		},
		sinkOptions = {},
		rangeCheck = true,
		totemColors = {
			{0.9, 0.4, 0}, -- [1] == Fire
			{0.3, 1, 0.3}, -- [2] == Earth
			{0, 0.7, 1}, -- [3] == Water
			{1, 0.6, 1}, -- [4] == Air
		}
	}
}

function SimpleTotems:GetOptions()
	local anchorPoints = {
		["TOPRIGHT"] = "TOPRIGHT",
		["TOPLEFT"] = "TOPLEFT",
		["BOTTOMRIGHT"] = "BOTTOMRIGHT",
		["BOTTOMLEFT"] = "BOTTOMLEFT",
		
		["TOP"] = "TOP",
		["BOTTOM"] = "BOTTOM",
		["RIGHT"] = "RIGHT",
		["LEFT"] = "LEFT",

		["CENTER"] = "CENTER",
	}

	local options = {
		name = "SimpleTotems",
		handler = SimpleTotems,
		type = 'group',
		childGroups = 'tree',
		get = function(info)
			return self.db.profile[info[#info]]
		end,
		set = function(info, value)
			self.db.profile[info[#info]] = value
			self:SetupFrames()
		end,
		args = {
			frame = {
				name = L["Size and scale"],
				type = 'group',
				order = 1,
				args = {
					iconSize = {
						type = 'range',
						name = L["Icon size"],
						min = 5,
						max = 50,
						step = 1,
						order = 1,
					},
					fontSize = {
						type = 'range',
						name = L["Font size"],
						min = 5,
						max = 50,
						step = 1,
						order = 2,
					},
					fontEffect = {
						type = 'select',
						name = L["Font Effect"],
						values = {
							none = L["None"],
							OUTLINE = L["Outline"],
							THICKOUTLINE = L["Thick outline"],
							MONOCHROME = L["Monochrome"],
						},
						order = 3,
					},
					scale = {
						type = 'range',
						name = L["Scale"],
						min = 0.1,
						max = 10,
						step = 0.1,
						order = 4,
					},
					xSpacing = {
						type = 'range',
						name = L["Horizontal Spacing"],
						min = -100,
						max = 100,
						step = 1,
						order = 5,
					},
					ySpacing = {
						type = 'range',
						name = L["Vertical Spacing"],
						min = -100,
						max = 100,
						step = 1,
						order = 6,
					},
				},
			},
			position = {
				name = L["Position"],
				type = 'group',
				order = 2,
				args = {
					point = {
						type = 'select',
						name = L["Anchor point"],
						desc = L["Anchor frame by which point on the frame"],
						values = anchorPoints,
						order = 1,
					},
					relativePoint = {
						type = 'select',
						name = L["Relative anchor"],
						desc = L["Anchor frame relative to which point on the screen"],
						values = anchorPoints,
						order = 2,
					},
					xOffset = {
						type = 'range',
						name = L["Horizontal offset"],
						min = -500,
						max = 500,
						step = 0.1,
						order = 3,
					},
					yOffset = {
						type = 'range',
						name = L["Vertical offset"],
						min = -500,
						max = 500,
						step = 0.1,
						order = 4,
					},
				},
			},
			display = {
				name = L["Display"],
				type = 'group',
				order = 3,
				args = {
					rangeCheck = {
						type = 'toggle',
						name = L["Range check"],
						desc = L["Paints the totem icon red when it's out of range"],
						set = function(_, val)
							self.db.profile.rangeCheck = val
							if val then
								self:RegisterEvent("PLAYER_AURAS_CHANGED", "ScanBuffs")
							else
								self:UnregisterEvent("PLAYER_AURAS_CHANGED")
							end
						end,
						order = 1,
					},
					borders = {
						type = 'toggle',
						name = L["Show borders"],
						order = 2,
						disabled = function() return LibStub("LibButtonFacade", true) end,
					},
					colorBorders = {
						type = 'toggle',
						name = L["Color borders"],
						order = 3,
					},
				},
			},
			totemColors = {
				name = L["Totem colors"],
				type = 'group',
				order = 4, 
				get = function(info) return unpack(self.db.profile.totemColors[tonumber(info[#info])]) end,
				set = function(info, r, g, b)
					local key = tonumber(info[#info])
					self.db.profile.totemColors[key][1] = r
					self.db.profile.totemColors[key][2] = g
					self.db.profile.totemColors[key][3] = b
					self:SetupFrames()
				end,
				args = {
					["1"] = {
						type = 'color',
						name = L["Fire totem color"],
						hasAlpha = false,
						order = 2,
					},
					["2"] = {
						type = 'color',
						name = L["Earth totem color"],
						hasAlpha = false,
						order = 1,
					},
					["3"] = {
						type = 'color',
						name = L["Water totem color"],
						hasAlpha = false,
						order = 3,
					},
					["4"] = {
						type = 'color',
						name = L["Air totem color"],
						hasAlpha = false,
						order = 4,
					},
				},
			},
			warning = {
				name = L["Warnings"],
				type = 'group',
				order = 5, 
				get = function(info) return self.db.profile.warning[info[#info]] end,
				args = {
					show = {
						type = 'toggle',
						name = L["Show warnings"],
						desc = L["Show a message when a totem expires"],
						set = function(info, val) self.db.profile.warning.show = val end,
						order = 1,
					},
					colorName = {
						type = 'toggle',
						name = L["Color totem name"],
						set = function(info, val) self.db.profile.warning.colorName = val end,
						disabled = function() return not self.db.profile.warning.show end,
						order = 2,
					},
					color = {
						type = 'color',
						name = L["Warning color"],
						get = function() return unpack(self.db.profile.warning.color) end,
						set = function(_, r, g, b)
							self.db.profile.warning.color[1] = r
							self.db.profile.warning.color[2] = g
							self.db.profile.warning.color[3] = b
						end,
						hasAlpha = false,
						disabled = function() return not self.db.profile.warning.show end,
						order = 3,
					},
					showIcon = {
						type = 'toggle',
						name = L["Show icon"],
						set = function(_, val)
							self.db.profile.warning.showIcon = not self.db.profile.warning.showIcon
						end,
						disabled = function() return not self.db.profile.warning.show end,
						order = 4,
					},
				},
			},
			moving = {
				type = 'toggle',
				name = L["Unlock frame"],
				get = function() return self.moving end,
				set = "MoveFrame",
				order = 21,
			},
		},
	}
	options.args.warning.args.sinkOptions = self:GetSinkAce3OptionsDataTable()
	options.args.profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
	return options
end
