# SimpleTotems
Simple totem timer addon for WoW TBC 2.4.3.

![Preview](https://i.imgur.com/SC9HE6N.png)

## Features
* Pulse timers, including Searing Totem's casts
* Range check (only totems with auras)
* Totem expiration messages
* Rightclick icon to destroy totem
* ButtonFacade support
* Very low resource usage
* /st to open a configuration GUI