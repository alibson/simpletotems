## Interface: 20400
## Title: SimpleTotems
## Version: 1.4.1
## Author: Alibson 
## Notes: Just a simple totem display
## SavedVariables: SimpleTotemsDB
## OptionalDeps: Ace3, LibSink-2.0, ButtonFacade
## X-Embeds: Ace3, LibSink-2.0
## X-Category: Shaman

embeds.xml

Locales\Locales.xml
SimpleTotems.lua
Frames.xml
Options.lua
